package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.BaseConst;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.pojo.UpmsUserRole;
import com.micro.fast.upms.service.UpmsUserRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
*
* @author lsy
*/
@Api("upmsUserRole")
@RestController
@RequestMapping("/upmsUserRole")
public class UpmsUserRoleController {

  @Autowired
  private UpmsUserRoleService<UpmsUserRole,Integer> upmsUserRoleService;

  @ApiOperation("添加信息")
  @PostMapping
  public ServerResponse addUpmsUserRole(@Valid UpmsUserRole upmsUserRole) throws BindException {
    return  upmsUserRoleService.add(upmsUserRole);
  }

  @ApiOperation("根据id查询详细信息")
  @GetMapping("/{id}")
  public ServerResponse getUpmsUserRoleById(
  @PathVariable(value = "id") @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    return upmsUserRoleService.getById(Integer.valueOf(id));
  }

  @ApiOperation("根据条件分页查询")
  @GetMapping
  public ServerResponse getUpmsUserRoleByCondition(UpmsUserRole upmsUserRole,
                                             @RequestParam(defaultValue = "1",required = false) int pageNum,
                                             @RequestParam(defaultValue = "10",required = false)int pageSize,
                                             @RequestParam(required = false) String orderBy){
    return upmsUserRoleService.getByCondition(upmsUserRole,pageNum,pageSize,orderBy);
  }

  @ApiOperation("修改信息")
  @PutMapping("/{id}")
  public ServerResponse updateUpmsUserRole(UpmsUserRole upmsUserRole,
                                     @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    upmsUserRole.setUserRoleId(Integer.valueOf(id));
    return upmsUserRoleService.update(upmsUserRole);
  }

  @ApiOperation("根据id删除，传入数组")
  @DeleteMapping
  public ServerResponse deleteUpmsUserRole(@NotEmpty(message = BaseConst.BASEMSG_PREFIX+"请传入ids") List<Integer> ids){
    return upmsUserRoleService.deleteByIds(ids);
  }
}

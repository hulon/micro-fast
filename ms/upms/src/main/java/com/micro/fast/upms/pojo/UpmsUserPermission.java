package com.micro.fast.upms.pojo;

import java.io.Serializable;

public class UpmsUserPermission implements Serializable {
    private Integer userPermissionId;

    private Integer userId;

    private Integer permissionId;

    private Byte type;

    private static final long serialVersionUID = 1L;

    public UpmsUserPermission(Integer userPermissionId, Integer userId, Integer permissionId, Byte type) {
        this.userPermissionId = userPermissionId;
        this.userId = userId;
        this.permissionId = permissionId;
        this.type = type;
    }

    public UpmsUserPermission() {
        super();
    }

    public Integer getUserPermissionId() {
        return userPermissionId;
    }

    public void setUserPermissionId(Integer userPermissionId) {
        this.userPermissionId = userPermissionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userPermissionId=").append(userPermissionId);
        sb.append(", userId=").append(userId);
        sb.append(", permissionId=").append(permissionId);
        sb.append(", type=").append(type);
        sb.append("]");
        return sb.toString();
    }
}
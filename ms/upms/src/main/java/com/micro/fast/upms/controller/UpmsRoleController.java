package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.BaseConst;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.pojo.UpmsRole;
import com.micro.fast.upms.service.UpmsRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
*
* @author lsy
*/
@Api("upmsRole")
@RestController
@RequestMapping("/upmsRole")
public class UpmsRoleController {

  @Autowired
  private UpmsRoleService<UpmsRole,Integer> upmsRoleService;

  @ApiOperation("添加信息")
  @PostMapping
  public ServerResponse addUpmsRole(@Valid UpmsRole upmsRole) throws BindException {
    return  upmsRoleService.add(upmsRole);
  }

  @ApiOperation("根据id查询详细信息")
  @GetMapping("/{id}")
  public ServerResponse getUpmsRoleById(
  @PathVariable(value = "id") @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    return upmsRoleService.getById(Integer.valueOf(id));
  }

  @ApiOperation("根据条件分页查询")
  @GetMapping
  public ServerResponse getUpmsRoleByCondition(UpmsRole upmsRole,
                                             @RequestParam(defaultValue = "1",required = false) int pageNum,
                                             @RequestParam(defaultValue = "10",required = false)int pageSize,
                                             @RequestParam(required = false) String orderBy){
    return upmsRoleService.getByCondition(upmsRole,pageNum,pageSize,orderBy);
  }

  @ApiOperation("修改信息")
  @PutMapping("/{id}")
  public ServerResponse updateUpmsRole(UpmsRole upmsRole,
                                     @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    upmsRole.setId(Integer.valueOf(id));
    return upmsRoleService.update(upmsRole);
  }

  @ApiOperation("根据id删除，传入数组")
  @DeleteMapping
  public ServerResponse deleteUpmsRole(@NotEmpty(message = BaseConst.BASEMSG_PREFIX+"请传入ids") List<Integer> ids){
    return upmsRoleService.deleteByIds(ids);
  }
}

#!/usr/bin/env bash
docker run -d   -p 10008:10008 -p 10009:10009 registry.cn-hangzhou.aliyuncs.com/lishouyu/hub:ms-upmsV0.0.1-SNAPSHOT \
 java -jar \
 -Deureka.client.serviceUrl.defaultZone=http://micro:fast@172.17.0.1:10002/eureka/,http://micro:fast@172.17.0.1:10004/eureka/ \
 -Deureka.instance.prefer-ip-address=true \
 -Deureka.instance.ip-address=172.17.0.1 \
 -Dspring.cloud.config.profile=deploy \
 /workhome/app.jar
